# Persistent Settings Generator

Generates an persistent observable model from a short description.
The state is saved automatically if something changes.

You can automate this via a custom gradle task.

## Example Usage

*I need some settings, some flags for the GUI, and the dimension of the GUI saved and observable.* 

Description in the *.sp file:

```
//this is a comment and will not transfer to the generated file
//package name of the generated settings file
package settings

//this is where java will store the persitent data (see Java Preferences API)
app=MySettingsNodeLocation
//name of the generated file
name=MySettings

// string
#testString   | String    | "default"

// boolean
#testBoolean  | Boolean   | false

// integer
#testInteger  | Integer   | 25

//you can also inject custom code verbatim
//start of custom code is marked by a '${' line
${
    public Boolean getTestFunctionTrue(){
        return true;
    }
}$
//end of custom code is marked by a '}$' line
```

This will generate a class *MySettings* located in the package *settings* with specified attributes 
which can be observed as follows:

```
#!java

//import MySettings...

public void test() throws Exception {
        {//initial program start
            SettingsGeneratorTest settings = new SettingsGeneratorTest();

            //load default value specified in the settings description file
            Assert.assertEquals((Integer) 25, settings.getTestInteger());
            
            //everything is available as a property
            settings.testIntegerProperty().addListener(c -> System.out.println("modified"));

            //modify, persisted automatically
            settings.setTestInteger(33);
        }
        //program closed
        {//program started again
            SettingsGeneratorTest settings = new SettingsGeneratorTest();

            //settings loaded, value modified
            Assert.assertEquals((Integer) 33, settings.getTestInteger());
        }
    }
```

## Supported Types

Currently supported types:

* Integer, Boolean, String (I,B,S)
* Custom code injection


## Gradle Setup

Currently, there is no maven repository for this module. You have to install it into 
your local maven repository.

* Install this module into your local maven repository via '*gradle install*'
* Include the module in your gradle project as follows:


```
#!gradle

...

String genDir = "$projectDir/src/generated/java"

sourceSets.main.java {
	srcDir genDir
	...
}

repositories {
    ...
    mavenLocal()
}

dependencies {
    ...
	compileOnly 'org.pivk.generators.configuration:persistent-settings-generator:0.+'
}

task( generateSet, type: JavaExec ) {
    main = "org.pivk.generators.configuration.PersistentPropertiesGenerator"
	classpath = configurations.compileOnly
	args = ["-i", "src/main/java/org/pivk/hman/srv/settings.sp", "-o", "src/generated/java/org/pivk/hman/srv/configuration/","-s"]
}

compileJava.dependsOn genSet

clean.doFirst {
    delete fileTree(dir: genDir)
}
```

'*gradle build*' now generates source files for persistent settings located in the '*src-generated*' folder.