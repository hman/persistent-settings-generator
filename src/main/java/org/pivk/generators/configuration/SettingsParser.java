package org.pivk.generators.configuration;

import javafx.util.Pair;
import org.pivk.util.FileIO;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

class SettingsParser {
    private AtomicLong snippetCounter = new AtomicLong();
    private boolean customCodeStarted = false;

    //references
    private final Map<String, Pair<String, String>> elements;
    private final Map<Long, StringBuilder> codeSnippets;
    private final Map<SchemeProperties, String> properties;

    SettingsParser(Map<SchemeProperties, String> properties,
                   Map<String, Pair<String, String>> elements, Map<Long, StringBuilder> codeSnippets) {
        this.properties = properties;
        this.elements = elements;
        this.codeSnippets = codeSnippets;
    }

    void parseSettingsFile(String inputFile) throws IOException {
        FileIO.forEachLine(inputFile, this::processLine);
    }

    private void processLine(String s) {
        if(s.startsWith("}$")){
            customCodeStarted = false;
            return;
        }

        if(customCodeStarted){
            StringBuilder code = codeSnippets.computeIfAbsent(snippetCounter.get(), k -> new StringBuilder());
            code.append(s).append("\n");
            return;
        }

        if(s.startsWith("package")){
            properties.put(SchemeProperties.PACKAGE, s);
        }else

        if(s.startsWith("app")){
            String app = s.split("=")[1].trim();
            properties.put(SchemeProperties.APPNAME, app);
        }else

        if(s.startsWith("name")){
            String name = s.split("=")[1].trim();
            properties.put(SchemeProperties.NAME, name);
        }else

        if(s.startsWith("${")){
            customCodeStarted = true;
            snippetCounter.incrementAndGet();
        }else

        if(s.startsWith("#")){
            String[] element = s.split("\\|");

            //remove whitespace and trailing #
            String name = element[0].trim().substring(1);
            String type = element[1].trim();
            String def  = element[2].trim();

            elements.put(name, new Pair<>(type, def));
        }
    }
}
