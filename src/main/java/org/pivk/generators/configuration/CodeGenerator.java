package org.pivk.generators.configuration;

import javafx.util.Pair;
import org.pivk.util.FileIO;
import org.pivk.util.StringUtil;

import java.io.*;
import java.net.URISyntaxException;
import java.util.Map;

class CodeGenerator {
    private final Map<String, Pair<String, String>> elements;
    private final Map<Long, StringBuilder> codeSnippets;
    private final Map<SchemeProperties, String> properties;
    private final boolean spring;

    CodeGenerator(Map<SchemeProperties, String> properties,
                  Map<String, Pair<String, String>> elements, Map<Long, StringBuilder> codeSnippets, boolean spring) {
        this.elements = elements;
        this.codeSnippets = codeSnippets;
        this.properties = properties;
        this.spring = spring;
    }

    void generateJava(String output) throws IOException, URISyntaxException {
        StringBuilder body = new StringBuilder();
        parseBody(body);

        FileIO.overwriteFile(
                new File(output+properties.get(SchemeProperties.NAME)+".java"),
                body.toString()
        );
    }

    private void parseBody(StringBuilder body) throws IOException, URISyntaxException {
        String pkg = properties.get(SchemeProperties.PACKAGE);
        String name = properties.get(SchemeProperties.NAME);
        String app = properties.get(SchemeProperties.APPNAME);

        String bb = FileIO.readBody(PersistentPropertiesGenerator.class.getResourceAsStream("Body.gen"));

        //Body      :: name, path, attributes, unwrap, wrap
        bb = bb.replace("$name$",name);
        bb = bb.replace("$app$",app);
        //TODO import statements
        bb = bb.replace("$import$","");

        StringBuilder customCode = new StringBuilder();
        for (Long id : codeSnippets.keySet()) {
            customCode.append(codeSnippets.get(id).toString()).append("\n\n");
        }

        bb = bb.replace("$custom-code$", customCode.toString());
        bb = bb.replace("$package$",pkg+";");

        StringBuilder parsedElements = new StringBuilder();

        elements.forEach((elementName,elementType) -> {
            try {
                parseElement(parsedElements,elementName,elementType.getKey(),elementType.getValue());
            } catch (IOException e) {
                throw new IllegalStateException("Errors in element found! "+elementName + " :: "+elementType);
            }
        });

        bb = bb.replace("$elements$", parsedElements.toString());

        if(spring){
            bb = bb.replace("$spring$",
                    "import org.springframework.stereotype.Component;\n\n@Component");
        } else{
            bb = bb.replace("$spring$", "");
        }

        body.append(bb);
    }

    private void parseElement(StringBuilder elements, String name, String type, String defValue) throws IOException {
        String bb = FileIO.readBody(PersistentPropertiesGenerator.class.getResourceAsStream("Element.gen"));

        String liftedName = StringUtil.lift(name);

        String prefType = type;
        if(prefType.equals("String")) prefType = "";
        else if (prefType.equals("Integer")) prefType = "Int";

        bb = bb.replace("$type$",type);
        bb = bb.replace("$pref-type$",prefType);
        bb = bb.replace("$Element$",liftedName);
        bb = bb.replace("$element$",name);
        bb = bb.replace("$default$",defValue);

        elements.append(bb);
    }
}