package org.pivk.generators.configuration;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.internal.Lists;
import javafx.util.Pair;

import java.io.*;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.beust.jcommander.Parameter;

/**
 * 2016/09/07
 * -
 * 2017/24/07
 */
@SuppressWarnings({"unused", "FieldCanBeLocal"})
public class PersistentPropertiesGenerator {
    @Parameter
    public List<String> parameters = Lists.newArrayList();

    @Parameter(names = {"--output", "-o"}, required = true, arity = 1,
            description = "Output folder where the generated files are stored")
    private String output;

    @Parameter(names = {"--input", "-i"}, required = true, arity = 1,
            description = "Input file in *.sp format")
    private String input;

    @Parameter(names = {"--spring-service", "-s"}, description = "Spring component and service output")
    private boolean spring = false;

    public static void main(String[] args) throws IOException, URISyntaxException {
        PersistentPropertiesGenerator jct = new PersistentPropertiesGenerator();
        JCommander.newBuilder()
                .addObject(jct)
                .build()
                .parse(args);

        jct.run();
    }

    private void run() throws IOException, URISyntaxException {
        String inputFile = input;
        String outputFolder = output;

        Map<SchemeProperties, String> properties = new HashMap<>();
        Map<String, Pair<String,String>> elements = new HashMap<>();
        Map<Long, StringBuilder> codeSnippets = new HashMap<>();

        SettingsParser parser = new SettingsParser(properties, elements, codeSnippets);
        parser.parseSettingsFile(inputFile);

        CodeGenerator generator = new CodeGenerator(properties, elements, codeSnippets, spring);
        generator.generateJava(outputFolder);
    }
}


