package org.pivk.generators.configuration;

import org.junit.Assert;
import org.junit.Test;

/**
 * 2016/09/08.
 */
public class Supported {

    @Test
    public void integerTest() {
        {//initial
            SettingsGeneratorTest settings = new SettingsGeneratorTest();

            Assert.assertEquals((Integer) 25, settings.getTestInteger());

            settings.setTestInteger(33); //persist
        }
        {//persisted settings
            SettingsGeneratorTest settings = new SettingsGeneratorTest();

            Assert.assertEquals((Integer) 33, settings.getTestInteger());

            settings.setTestInteger(25); //restore default

        }
    }

    @Test
    public void stringTest() {
        {//initial
            SettingsGeneratorTest settings = new SettingsGeneratorTest();

            Assert.assertEquals("default", settings.getTestString());

            settings.setTestString("changed!"); //persist
        }
        {//persisted settings
            SettingsGeneratorTest settings = new SettingsGeneratorTest();

            Assert.assertEquals("changed!", settings.getTestString());

            settings.setTestString("default"); //restore default
        }
    }

    @Test
    public void booleanTest() {
        {//initial
            SettingsGeneratorTest settings = new SettingsGeneratorTest();

            Assert.assertFalse(settings.getTestBoolean());

            settings.setTestBoolean(true);
        }
        {//persisted settings
            SettingsGeneratorTest settings = new SettingsGeneratorTest();

            Assert.assertTrue(settings.getTestBoolean());

            settings.setTestBoolean(false); //restore default
        }

    }

    @Test
    public void customFunction() {
        SettingsGeneratorTest settings = new SettingsGeneratorTest();
        Assert.assertTrue(settings.getTestFunctionTrue());
    }

    @Test
    public void customComplexFunction() {
        SettingsGeneratorTest settings = new SettingsGeneratorTest();

        Assert.assertEquals(SettingsGeneratorTest.testEnum.TESTENUM, settings.getTestEnum());
    }
}
