package org.pivk.generators.configuration;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.collections.ObservableSet;
import javafx.beans.property.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.prefs.Preferences;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class SettingsGeneratorTest{
    private static final Logger l = LoggerFactory.getLogger("SettingsGeneratorTest");
    public static final String NODE = "settingsTest";
    private Preferences prefs = Preferences.userRoot().node(SettingsGeneratorTest.NODE);

    // GENERATED CODE
        // testBoolean PERSISTENT PROPERTY

    { // load on init and save on change
        loadTestBoolean();
        testBooleanProperty().addListener(c -> saveTestBoolean());
    }

    //node location
    public static final String testBoolean_node = "testBoolean";

    private BooleanProperty testBoolean;
    public Boolean getTestBoolean() { return testBooleanProperty().get(); }
    public void setTestBoolean(Boolean testBoolean) { testBooleanProperty().set(testBoolean); }
    public BooleanProperty testBooleanProperty() {
        if(testBoolean == null) testBoolean = new SimpleBooleanProperty();
        return testBoolean;
    }

    //save and load
    private void saveTestBoolean() {
        l.trace("Saving testBoolean '{}'", testBooleanProperty().get());
        prefs.putBoolean(testBoolean_node, testBoolean.get());
    }

    private void loadTestBoolean() {
        Boolean testBoolean = prefs.getBoolean(testBoolean_node, false);
        l.info("Loading testBoolean '{}'", testBoolean);
        testBooleanProperty().set(testBoolean);
    }
    // testInteger PERSISTENT PROPERTY

    { // load on init and save on change
        loadTestInteger();
        testIntegerProperty().addListener(c -> saveTestInteger());
    }

    //node location
    public static final String testInteger_node = "testInteger";

    private IntegerProperty testInteger;
    public Integer getTestInteger() { return testIntegerProperty().get(); }
    public void setTestInteger(Integer testInteger) { testIntegerProperty().set(testInteger); }
    public IntegerProperty testIntegerProperty() {
        if(testInteger == null) testInteger = new SimpleIntegerProperty();
        return testInteger;
    }

    //save and load
    private void saveTestInteger() {
        l.trace("Saving testInteger '{}'", testIntegerProperty().get());
        prefs.putInt(testInteger_node, testInteger.get());
    }

    private void loadTestInteger() {
        Integer testInteger = prefs.getInt(testInteger_node, 25);
        l.info("Loading testInteger '{}'", testInteger);
        testIntegerProperty().set(testInteger);
    }
    // testString PERSISTENT PROPERTY

    { // load on init and save on change
        loadTestString();
        testStringProperty().addListener(c -> saveTestString());
    }

    //node location
    public static final String testString_node = "testString";

    private StringProperty testString;
    public String getTestString() { return testStringProperty().get(); }
    public void setTestString(String testString) { testStringProperty().set(testString); }
    public StringProperty testStringProperty() {
        if(testString == null) testString = new SimpleStringProperty();
        return testString;
    }

    //save and load
    private void saveTestString() {
        l.trace("Saving testString '{}'", testStringProperty().get());
        prefs.put(testString_node, testString.get());
    }

    private void loadTestString() {
        String testString = prefs.get(testString_node, "default");
        l.info("Loading testString '{}'", testString);
        testStringProperty().set(testString);
    }


    // CUSTOM CODE
    public Boolean getTestFunctionTrue(){
        return true;
    }


    public enum testEnum{
        TESTENUM
    }

    public testEnum getTestEnum(){
        return testEnum.TESTENUM;
    }



}
