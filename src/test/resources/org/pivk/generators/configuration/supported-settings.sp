package org.pivk.generators.configuration
//node save location (see Java Preferences API)
app=settingsTest
name=SettingsGeneratorTest
//separates implementation and interface, annotates with spring stereotypes
springService=true

// -------------------------------------
//              imports
// -------------------------------------
//TODO implement

// -------------------------------------
//              annotations
// -------------------------------------
//TODO implement

// -------------------------------------
//              settings
// -------------------------------------

// string
#testString   | String    | "default"

// boolean
#testBoolean  | Boolean   | false

// integer
#testInteger  | Integer   | 25


// -------------------------------------
//          CUSTOM CODE
// -------------------------------------


${
    public Boolean getTestFunctionTrue(){
        return true;
    }
}$


${
    public enum testEnum{
        TESTENUM
    }

    public testEnum getTestEnum(){
        return testEnum.TESTENUM;
    }
}$

